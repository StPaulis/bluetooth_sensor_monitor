var axios = require('axios');

var connectionString = process.env.CONN_STRING;

var clientFromConnectionString = require('azure-iot-device-http').clientFromConnectionString;
var Message = require('azure-iot-device').Message;

var client = clientFromConnectionString(connectionString);

var isRunning = 0;

var connectCallback = function (err) {
  if (err) {
    console.error('Could not connect: ' + err);
  } else {
    console.log('Client connected ');
    var message = new Message('some data from my device');
    client.sendEvent(message, function (err) {
      if (err) console.log(err.toString());
    });

    client.on('message', function (msg) {
      if (msg.data.endsWith("on")) {
        var nodeId = parseInt(msg.data)
        console.log('Status ' + process.env.POST_URL, nodeId );
        axios.put(`http://${process.env.MONITOR_IP}:60080/read_values`)
          .then(() => console.log('power on monitor'))
          .catch(() => console.log('error powering on'));
        isRunning = setInterval(function dealWithApi() {
          axios.get(`http://${process.env.MONITOR_IP}:60080/values`)
            .then(function (response) {
              axios.post(`http://${process.env.POST_URL}`, {
                dataTypeId: 1, nodeId: nodeId
                , value: response.data.barometricPressure.temperature.value, unit: "C", description: "temp from bluetooth sensor CC2650STK"
              })
                .then(function (res) {
                  res.data.lenght > 100 ? console.log(res.data) : console.log('Huge message has been received from monitor service') ;
                })
                .catch(function (error) {
                  console.log('error send data to api');
                });
              axios.post(`http://${process.env.POST_URL}`, {
                dataTypeId: 2, nodeId: nodeId
                , value: response.data.humidity.relativeHumidity.value, unit: "%RH", description: "humidity from bluetooth sensor CC2650STK"
              })
                .then(function (res) {
                  res.data.lenght > 100 ? console.log(res.data) : console.log('Huge message has been received from monitor service') ;
                })
                .catch(function () {
                  console.log('error send data to api');
                });
            })
            .catch(function () {
              console.log('error getting values from monitor');
            });
        }, 10000);
      } else if (msg.data === 'off') {
        axios.delete(`http://${process.env.MONITOR_IP}:60080/read_values`)
          .then(() => console.log('power off monitor'))
          .catch(() => console.log('error powering off'));
        clearInterval(isRunning);
      }

      client.complete(msg, function () {
        console.log('completed');
      });
    });
  }
};

client.open(connectCallback);


